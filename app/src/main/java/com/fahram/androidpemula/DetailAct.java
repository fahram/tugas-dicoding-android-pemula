package com.fahram.androidpemula;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailAct extends AppCompatActivity {

    Academy academy;
    TextView tvName, tvPrice, tvAuthor, tvLevel, tvDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        academy = getIntent().getParcelableExtra("ACADEMY");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tvName = findViewById(R.id.tvDetailName);
        tvPrice = findViewById(R.id.tvDetailPrice);
        tvAuthor = findViewById(R.id.tvAuthor);
        tvLevel = findViewById(R.id.tvLevel);
        tvDescription = findViewById(R.id.tvDescription);
        ImageView imageView = findViewById(R.id.ivDetail);
        tvName.setText(academy.getName());
        tvPrice.setText(academy.getPrice());
        tvAuthor.setText(academy.getAuthor());
        tvLevel.setText(academy.getLevel());
        tvDescription.setText(academy.getDescription());
        Glide.with(this)
                .load(academy.getImage())
                .placeholder(R.drawable.placeholder)
                .into(imageView);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
