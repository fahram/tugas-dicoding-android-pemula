package com.fahram.androidpemula;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<Academy> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.rView);
        recyclerView.setHasFixedSize(true);

        list.addAll(Data.getListData());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        AcademyAdapter academyAdapter = new AcademyAdapter(this, new AcademyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Academy item) {
                Intent intent = new Intent(MainActivity.this, DetailAct.class);
                intent.putExtra("ACADEMY", item);
                startActivity(intent);
            }
        });
        academyAdapter.setAcademyArrayList(list);
        recyclerView.setAdapter(academyAdapter);

    }
}
