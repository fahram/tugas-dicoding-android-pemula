package com.fahram.androidpemula;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by fahram on 2019-05-24
 */
public class AcademyAdapter extends RecyclerView.Adapter<AcademyAdapter.AcademyViewHolder> {
    public Context context;
    private ArrayList<Academy> academyArrayList;

    public interface OnItemClickListener {
        void onItemClick(Academy item);
    }

    OnItemClickListener listener;

    private ArrayList<Academy> getAcademyArrayList() {
        return academyArrayList;
    }

    void setAcademyArrayList(ArrayList<Academy> academyArrayList) {
        this.academyArrayList = academyArrayList;
    }

    AcademyAdapter(Context context, OnItemClickListener listener) {
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public AcademyViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
        View itemRow = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list, viewGroup, false);
        return new AcademyViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(@NonNull AcademyViewHolder academyViewHolder, final int i) {
        final Academy academy = getAcademyArrayList().get(i);
        academyViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(academy);
            }
        });
        academyViewHolder.tvName.setText(academy.getName());
        academyViewHolder.tvPrice.setText(academy.getPrice());
        Glide.with(context)
                .load(getAcademyArrayList().get(i).getImage())
                .placeholder(R.drawable.placeholder)
                .into(academyViewHolder.imageView);

        academyViewHolder.btnFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Favorite " + academy.getName(), Toast.LENGTH_SHORT).show();
            }
        });

        academyViewHolder.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Register " + academy.getName(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return getAcademyArrayList().size();
    }

    public class AcademyViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvPrice;
        ImageView imageView;
        Button btnFavorite, btnRegister;

        AcademyViewHolder(@NonNull final View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            imageView = itemView.findViewById(R.id.ivDicoding);
            btnFavorite = itemView.findViewById(R.id.btnFavorite);
            btnRegister = itemView.findViewById(R.id.btnRegister);

        }

    }
}
