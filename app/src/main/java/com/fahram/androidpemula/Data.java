package com.fahram.androidpemula;

import java.util.ArrayList;

/**
 * Created by fahram on 2019-05-24
 */
public class Data {
    public static String[][] data = new String[][]{
            {
                    "Belajar Membuat Aplikasi Android untuk Pemula",
                    "Rp 400.000 (400 Pts)",
                    "https://www.dicoding.com/images/original/academy/belajar_membuat_aplikasi_android_untuk_pemula_logo_070119140911.jpg",
                    "Belajar Android untuk Pemula dengan Google Authorized Training Partner selama 30 hari!",
                    "oleh: Google ATP",
                    "Pemula"
            },
            {
                    "Memulai Pemrograman Dengan Kotlin",
                    "Rp 400.000 (400 Pts)",
                    "https://www.dicoding.com/images/original/academy/kotlin_fundamental_logo_010219112628.jpg",
                    "Kelas pengantar untuk mempelajari Functional Programming serta Object-Oriented Programming menggunakan Kotlin",
                    "oleh: Dicoding Indonesia",
                    "Pemula"
            },
            {
                    "Belajar Membuat Game untuk Pemula",
                    "Rp 400.000 (400 Pts)",
                    "https://www.dicoding.com/images/original/academy/belajar_membuat_game_untuk_pemula_logo_070119140848.jpg",
                    "Belajar Mengembangkan Game dengan Kurikulum dari Asosiasi Game Indonesia selama 30 Hari!",
                    "oleh: Asosiasi Game Indonesia",
                    "Pemula"
            },
            {
                    "Belajar Menjadi AWS Solutions Architect Associate",
                    "Rp 400.000 (400 Pts)",
                    "https://www.dicoding.com/images/original/academy/cloud_academy_logo_310119223939.jpg",
                    "Belajar Teknologi AWS: EC2, EBS, Route53, VPC, dan lainnya",
                    "oleh: Dicoding Indonesia",
                    "Menengah"
            },
            {
                    "Menjadi Android Developer Expert",
                    "Rp 2.200.000 (2200Pts)",
                    "https://www.dicoding.com/images/original/academy/menjadi_android_developer_expert_logo_070119140352.jpg",
                    "Jadilah expert di dunia pemrograman Android. Materi disusun oleh Dicoding sebagai Google Authorized Training Partner.",
                    "oleh: Google ATP",
                    "Menengah"
            },
            {
                    "Menjadi Game Developer Expert",
                    "Rp 2.200.000 (2200Pts)",
                    "https://www.dicoding.com/images/original/academy/menjadi_game_developer_expert_logo_070119140532.jpg",
                    "Jadilah seorang Game dev pro. Dapatkan pengalaman bikin 8 jenis Game di kelas ini, sekarang!",
                    "oleh: Asosiasi Game Indonesia",
                    "Menengah"
            },
            {
                    "Kotlin Android Developer Expert",
                    "Rp 1.100.000 (1100Pts)",
                    "https://www.dicoding.com/images/original/academy/kotlin_android_developer_expert_logo_070119140227.jpg",
                    "Kuasai Kotlin, bahasa pemrograman yang didukung penuh Google untuk mengembangkan aplikasi Android.",
                    "oleh: Dicoding Indonesia",
                    "Menengah"
            },
            {
                    "Menjadi Flutter Developer Expert",
                    "Rp 1.650.000 (1650Pts)",
                    "https://www.dicoding.com/images/original/academy/flutter_academy_logo_140319093702.jpg",
                    "Jadilah developer yang dapat membuat aplikasi cross-platform (Android, iOS) yang cantik dan juga mudah.",
                    "oleh: GITS Indonesia",
                    "Menengah"
            },
            {
                    "Menjadi Construct 2 Developer Expert",
                    "Rp 1.100.000 (1100Pts)",
                    "https://www.dicoding.com/images/original/academy/menjadi_construct2_developer_expert_logo_250419152917.jpg",
                    "Dapatkan pengalaman membuat game online dengan berbagai macam teknik lainnya",
                    "oleh: Dicoding Indonesia",
                    "Menengah"
            },
            {
                    "Membangun Progressive Web Apps",
                    "Rp 1.100.000 (1100Pts)",
                    "https://www.dicoding.com/images/original/academy/membangun_progressive_web_apps_logo_070119142922.jpg",
                    "Mari belajar Progressive Web App, website kekinian dengan fungsionalitas seperti desktop / mobile App!",
                    "oleh: CodePolitan",
                    "Menengah"
            },
            {
                    "Menjadi Azure Cloud Developer",
                    "Rp 1.100.000 (1100Pts)",
                    "https://www.dicoding.com/images/original/academy/azure_cloud_for_developer_logo_210219114249.jpg",
                    "Jadilah Cloud Developer Sekarang! Powered by Microsoft Azure",
                    "oleh: Dicoding Indonesia",
                    "Menengah"
            }
    };

    public static ArrayList<Academy> getListData() {
        Academy academy = null;
        ArrayList<Academy> list = new ArrayList<>();
        for (String[] aData : data){
            academy = new Academy();
            academy.setName(aData[0]);
            academy.setPrice(aData[1]);
            academy.setImage(aData[2]);
            academy.setDescription(aData[3]);
            academy.setAuthor(aData[4]);
            academy.setLevel(aData[5]);
            list.add(academy);
        }
        return  list;
    }
}
