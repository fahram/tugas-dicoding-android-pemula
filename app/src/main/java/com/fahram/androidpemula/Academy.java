package com.fahram.androidpemula;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by fahram on 2019-05-24
 */
public class Academy implements Parcelable {

    private String name, image, price , description, author, level;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Academy() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.image);
        dest.writeString(this.price);
        dest.writeString(this.description);
        dest.writeString(this.author);
        dest.writeString(this.level);
    }

    protected Academy(Parcel in) {
        this.name = in.readString();
        this.image = in.readString();
        this.price = in.readString();
        this.description = in.readString();
        this.author = in.readString();
        this.level = in.readString();
    }

    public static final Creator<Academy> CREATOR = new Creator<Academy>() {
        @Override
        public Academy createFromParcel(Parcel source) {
            return new Academy(source);
        }

        @Override
        public Academy[] newArray(int size) {
            return new Academy[size];
        }
    };
}
